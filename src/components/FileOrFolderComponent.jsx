import { useState } from "react"

export default function FileOrFolderComponent({
    name,
    type,
    path,
    refreshDirs,
    isSelected,
    toggleSelectedDir
}){

    // let [selected, setSelected] = useState(isSelected)

    return (
        <div
            className="file-or-folder-component"
            style={{
                border: isSelected ? "2px solid rgba(0, 256, 0, 0.4)" : "1px solid rgba(0,0,0,0.1)",
                width: "150px",
                height: "150px"
            }}
            onClick={() => toggleSelectedDir(path)}
            onDoubleClick={() => refreshDirs(path)}
        >
            <img
                src={type === "file" ? "/src/assets/file.svg" : "/src/assets/folder.svg"}
                alt=""
                width="50"
                height="50"
                style={{
                    display: "block",
                    marginLeft: "auto",
                    marginRight: "auto",
                }}
            ></img>
            <span>{name}</span>
        </div>
    )
}