import FileOrFolderComponent from "./components/FileOrFolderComponent"
import { useState, useEffect } from "react"
import axios, {isCancel, AxiosError} from 'axios'
import { Button, Alert } from 'react-bootstrap';
import 'bootstrap/dist/css/bootstrap.min.css';

axios.defaults.baseURL = 'http://127.0.0.1:8000'


function App() {

  const [dirs, setDirs] = useState([])
  const [parrentDir, setParrentDir] = useState("")
  const [currentDir, setCurrentDir] = useState("")
  const [selectedDirs, setSelectedDirs] = useState([])
  const [errorMessage, setErrorMessage] = useState("");

  const refreshDirs = (currentPath) => {
    const url = currentPath ? `/dirs?path=${currentPath}` : "/dirs"
    axios.get(url).then( response => {
      setDirs(response.data.dirs)
      setCurrentDir(response.data.current_dir)
      setParrentDir(response.data.parrent_dir)
    })
  }

  const toggleSelectedDir = (dir) => {
    const newSelectedDirs = selectedDirs.slice(0);
    const index = newSelectedDirs.indexOf(dir)
    if (index >= 0){
      newSelectedDirs.splice( index, 1 )
    }
    else {
      newSelectedDirs.push(dir)
    }
    setSelectedDirs(newSelectedDirs)
  }

  const showErrorMessage = (errorMessage) => {
    if (errorMessage) {
      setErrorMessage(errorMessage)
      setTimeout(() => {
        setErrorMessage("")
      }, 2000)
    }
  }

  useEffect(() => refreshDirs(), [])

  return (

    <div style={{display: "grid", gridTemplateColumns: "1fr 1fr", gap: "25px"}}>

      <div>
        <div style={{display: "grid", gridTemplateColumns: "1fr 3fr", gap: "25px"}}>
          <Button
            variant="success"
            onClick={()=> refreshDirs(parrentDir)}
            style={{marginBottom: "25px"}}
          >Назад</Button>
          <p style={{fontSize: "1.2rem"}}>{currentDir}</p>
        </div>

        <div style={{display: "flex", gap: "25px", flexWrap: "wrap"}}>
          {dirs.map( dir =>
            <FileOrFolderComponent
              type={dir.type}
              key={dir.name}
              name={dir.name}
              path={dir.path}
              refreshDirs={refreshDirs}
              toggleSelectedDir={toggleSelectedDir}
              isSelected={selectedDirs.indexOf(dir.path) >= 0 ? true: false}
            />)}
        </div>
      </div>

      <div>
        <div>
          <p style={{fontSize: "1.1rem"}}>Выбранные папки:</p>
          {selectedDirs.map(selectedDir => <p key={selectedDir}>{selectedDir}</p>)}
        </div>
        <Button
          variant="success"
          style={{position: "absolute", right: "25px", top: "25px", paddingLeft: "60px", paddingRight: "60px"}}
          onClick={() => {
            axios({
              method: 'post',
              url: "/composition",
              data: {selected_folders: selectedDirs},
              headers: {
                "Content-type": "application/json; charset=UTF-8",
                // "Accept": 'application/octet-stream'
              },
              responseType: "blob"
            })
            .then( response => {

              const href = window.URL.createObjectURL(response.data);
              const anchorElement = document.createElement('a');
              anchorElement.href = href;
              anchorElement.download = "Result.tif";
              document.body.appendChild(anchorElement);
              anchorElement.click();
              document.body.removeChild(anchorElement);
              window.URL.revokeObjectURL(href);

            })
            .catch( error => {
              showErrorMessage("Ошибка!")
            })
          }}
        >Скомпоновать файлы</Button>

        <Alert
          key="danger"
          variant="danger"
          show={errorMessage ? true: false}
          style={{position: "absolute", top: "0"}}
        >
          {errorMessage}
        </Alert>

      </div>

    </div>
  )
}

export default App
